<?php
//1 tạo đường dẫn thư mục, biến toàn cục
$web_root = 'http://'.$_SERVER['HTTP_HOST'];
define('__WEB_ROOR',$web_root);
define('__DIR_ROOR',__DIR__);
/*2. tự động load config 31/3/2022 kiếm tra file tồn tại thì 
require_once vào đỡ phải require nhiều lần.
khai báo kết nối db;
khai báo router;
*/
$scan_dir = scandir('configs');
if(isset($scan_dir))
{
    foreach($scan_dir as $itemfile)
    {
        if($itemfile!='.' && $itemfile !='..' && file_exists('configs/'.$itemfile))
        {
            require_once('configs/'.$itemfile);
        }
    }
}
/*3.so sánh đường dẫn ảo tương ứng với router được cấu hình và 
trỏ về file controller thật
*/
require_once('core/Router.php');
// thư viện chung
require_once('app/Helper.php');

// tự động load database
$db_config =  array_filter($config['database']) ;

if(!empty($db_config))
{
   require_once('core/Connection.php');
   require_once('core/QueryBuilder.php');
   require_once('core/Database.php');
   require_once('core/DB.php');
}
require_once('core/Model.php');
require_once('app/App.php');//
require_once('core/Controller.php');

?>