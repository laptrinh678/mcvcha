<?php
$route['default_controller'] ='home';
/*
	muc dich file nay tao duong dan than thien voi google,
	duong dan ao => dẫn đến file thật sau khi chạy qua class Router để kiểm tra và so sánh;
*/
$route['san-pham'] ='product/index';
$route['danh-muc-san-pham'] ='product/cate';
$route['trang-chu'] ='home/index';
$route['shop'] ='shop/index';
$route['shop-detail/(.+)'] ='shop/detail/$1';
$route['tin-tuc/(.+)'] ='news/category/$1';
$route['user'] ='user/index';
$route['user/(.+)'] ='user/show/$1';
$route['user/delete/(.+)'] ='user/delete/$1';
$route['user/create'] ='user/create';
$route['user/add'] ='user/add';
$route['user/edit/(.+)'] ='user/edit/$1';
$route['user/edit/post'] ='user/post';
$route['user/search'] ='user/search';
?>