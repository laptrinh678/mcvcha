<?php
/*
xây dựng hàm để điều hướng request đến các controller tương ứng;
load ra lỗi nếu không tìm thấy controller;
*/
class App{
    private $__controller;
    private $__action;
    private $param;
    private $__router;
    //private $db_class_App;
    function __construct()
    {
        global $route, $config;
        if(isset($route['default_controller']))
        {
            $this->__controller =$route['default_controller'];
            $this->__router = new Router;
            //print_r($config);
        }
       
        $this->__action ='index';
        $this->param =[];
        //$dbClassDB = new DB;
        //dd($dbClassDB);
        /* 
        b1.khoi tao doi tuong cua class DB trong thu muc core;
        b2.Khai bao thuoc tinh $db_class_App cua class App
        b3.gan thuoc tinh $db_class_App cua class App bang thuoc tinh db cua class DB;
        di chuyen den buoc 4 cung file
        */ 
        //$this->db_class_App = $dbClassDB;
        //dd($this->db_class_App);
        $this->hanldleUrl();
    }
    public function getUrl()
    {
        if(!empty($_SERVER['PATH_INFO']))
        {
            $url = $_SERVER['PATH_INFO'];
        }else{
            $url ='/';
        }
        return $url;

    }
    public function hanldleUrl()
    {
        $url_old = $this->getUrl();
        $url = $this->__router->hanlderRoute($url_old);
        $urlArr = array_filter(explode('/', $url));
        $urlArr = array_values($urlArr);
       // print_r($urlArr); die();
        // xử lý controller 20/2/2022 yen nghia
        if(!empty($urlArr[0]))
        {
            $this->__controller = ucfirst($urlArr[0]) ;
        }else
        {
            $length_01 = strlen($url_old);
            if($length_01 >1)
            {
                $this->loadError('Router:' . $url_old);
                die();
            }else{
                $this->__controller = ucfirst($this->__controller);
            }
        }
        if(file_exists('app/controllers/'.($this->__controller).'.php'))
            {
                require_once('controllers/'.($this->__controller).'.php');
                $this->__controller = new $this->__controller;
                //var_dump($this->__controller);die();
                $controller_name = $this->__controller;
                /*
                b4.Gan thuoc tinh $this->__controller->db cua class Controller
                bang thuoc tinh $this->db_class_App cua class App
                lam nhu nay thi class Controller se luon ket noi duoc den DB;
                 */
                $this->__controller->db = new DB;
                unset($urlArr[0]);
            }else{
                $this->loadError('Controller:' . $this->__controller);
            }   
            
        // xu ly action
        //var_dump($urlArr[1]); die();
        if(!empty($urlArr[1])){
            $this->__action = $urlArr[1];
            unset($urlArr[1]);
        }else{
            $this->__action ='index';
        }
        $this->param = array_values($urlArr);
       // tham so truyen vao cua ham:1 mảng [ten doi tuong, action xu ly],2: param của url;
       if(method_exists($this->__controller,$this->__action))
       {
        call_user_func_array([$this->__controller, $this->__action],$this->param);
       }else{
           $this->loadError('method:'.$this->__action);
       }
      
    }
    public function loadError($name)
    {
        require_once('errors/404.php');
    }
    
}


?>