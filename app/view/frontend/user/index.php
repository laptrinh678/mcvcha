<h3> list user</h3>
<hr>
<div class="col-md-12">
  <p class="text-right"><a href="<?php echo url('user/create/'); ?>" class="btn btn-success">Add</a></p>
  <div >
    <form action="<?php echo url('user/search/'); ?>">
      <div class="col-sm-3">
      <input type="text" class="form-control" name="email" >
      </div>
      <div class="col-sm-1"> <button class="btn btn-success">Search</button></div>
    </form>
    
  </div>
  <br>
  <br>
   <p style="color:red">
     <?php 
      if(isset($_SESSION['delete'])){ echo $_SESSION['delete']; unset($_SESSION['delete']);} 
      if(isset($_SESSION['add'])){ echo $_SESSION['add']; unset($_SESSION['add']);} 
      if(isset($_SESSION['update'])){ echo $_SESSION['update']; unset($_SESSION['update']);} 
     ?>
  </p>
</div>

<table class="table table-bordered">
    <thead>
      <tr>
        <th>Firstname</th>
        <th>Email</th>
        <th>Password</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach($user as $k=>$v) { ;?>
      <tr>
        <td><?php  echo $k;?> </td>
        <td><?php  echo $v['email']; ?></td>
        <td><?php  echo $v['password']; ?></td>
        <td>
            <a class="btn btn-danger" href="<?php echo url('user/delete/'); ?><?php echo $v['id']; ?>" >Delete</a>
            <a class="btn btn-success" href="<?php echo url('user/'); ?><?php echo $v['id']; ?>">View</a>
            <a class="btn btn-primary" href="<?php echo url('user/edit/'); ?><?php echo $v['id']; ?>">Edit</a>
        </td>
      </tr>
     <?php }; ?>
    </tbody>
  </table>