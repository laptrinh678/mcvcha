<p>Edit data</p>
<form action="<?php echo url('user/edit/post'); ?>" method="post">
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input class="form-control" name="email" value="<?php echo $user[0]['email']; ?>" id="exampleInputEmail1"
            aria-describedby="emailHelp" placeholder="Enter email">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input class="form-control" value="<?php echo $user[0]['password']; ?>" name="password"
            id="exampleInputPassword1" placeholder="Password">
    </div>
    <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
    </div>
    <input type="hidden" name="id" value="<?php echo $user[0]['id']; ?>">
    <button type="submit" class="btn btn-primary">Submit</button>
</form>