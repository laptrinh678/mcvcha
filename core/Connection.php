<?php 
class Connection
{
    private static $instance = null;
    private static $conn = null;
    /*
        sử dụng panerdesign singerton để tạo kết nối đến DB
        Giúp cho mỗi lần query không phải kết nối nhiều lần
        Do có function getInstance check, nếu đã kết nối rồi thì kg cho kết nối nữa
        nếu đã kêt nối rồi thì thực thi luôn.
        Làm cho việc truy vấn dữ liệu trở lên tốt hơn;
        tránh việc trang home có 10 khu vực lấy dữ liệu thì phải kết nối đến db 10 lần rồi query;
        thay vào đó chỉ phải kết nối 1 lần:
        Ngoài ra mẫu singel ton này cũng ứng dụng trong các chức năng khác 
        kiểu như check kết nối đên api...
    */
    private function __construct($db_config)
    {
        try
        {
        $dns = "mysql:host=".$db_config['host'].';'.'dbname='.$db_config['db'];
        $username = $db_config['user'];
        $con = new PDO($dns, $username, '');
        self::$conn = $con;
        }catch (PDOException $e) 
        {
            $data_err = "Kết nối thất bại: " . $e->getMessage();
            echo error($data_err); die();
        }
    }
    public static function getInstance($db_config)
    {
        if (static::$instance == null) 
        {
            $connection = new Connection($db_config);
            self::$instance = self::$conn;
        }
        return static::$instance;
    }
}
?>