<?php
trait QueryBuilder
{
   
    private $tabelName='';
    private $where='';
    private $field ='';
    private $operator ='';
    private $like ='';
    private $limit ='';
    private $orDerBy ='';
    private $innerJoin ='';
    private $leftJoin ='';
    private $rightJoin ='';

    public function tabel($tabelName)
    {
       // dd($tabelName);
        $this->tabelName = $tabelName;
        return $this;
        //return $tabelName;
    }
    public function where($field, $compare, $value)
    {
        if(empty($this->where))
        {
            $this->operator = ' WHERE ';

        }else{
            $this->operator = ' AND ';
        }
        $this->where .=  $this->operator .$field.' '.$compare.' '.$value;
        //dd($this->where);
        return $this;
    }
    public function select($field)
    {
       // dd($field);
        if($field==''){
             $this->field = '*';
             return $this;
        }else{
             $this->field = $field;
             return $this;
        }
    }
    public function like($field, $value)
    {
        $this->like = ' WHERE '.$field. ' LIKE ' ."'".$value."'";
        return $this;
    }
    public function orDerBy($field, $value='ASC')
    {
        $this->orDerBy = ' ORDER BY ' .$field. ' '. $value;
        return $this;

    }
    public function limit($value)
    {
        $this->limit = ' LIMIT '. $value;
        return $this;

    }
    // inner join
    public function innerJoin($tabelName, $relationship)
    {
        //SELECT * FROM sinhvien INNER JOIN lop ON sinhvien.LopID = lop.LopID;
        $this->innerJoin.= ' INNER JOIN ' .$tabelName .' ON '.$relationship. ' ';
        return $this;
    }
    public function leftJoin($tabelName, $relationship)
    {
        //SELECT * FROM students JOIN class ON students.class_id = class.class_id
        $this->leftJoin.= ' JOIN '. $tabelName .' ON '.$relationship;
        return $this;
    }
    public function rightJoin($tabelName, $relationship)
    {
        //ELECT ID, TEN, SOTIEN, NGAY FROM NHANVIEN RIGHT JOIN TIENTHUONG ON NHANVIEN.ID = TIENTHUONG.NHANVIEN_ID
        $this->rightJoin.= 'RIGHT JOIN'.$tabelName.'ON'.$relationship;
    }

    public function getAll()
    {
     
        $sqlquery = 'SELECT '.$this->field .' FROM '. $this->tabelName. $this->where. $this->like .$this->orDerBy . $this->limit .$this->innerJoin .$this->leftJoin;
        $data = $this->db->query($sqlquery);
        return $data;
    }
    public function create()
    {
        
    }
}
/*
Quy trinh global querybuild;
b1. khoi tao class Db va import querybuild vao
b2. vao file boottrap  require_once('core/DB.php');
b3. vao file App.php khoi tao doi tuong db
b4. tao thuoc tinh  public $db; trong core Controller;
b5. Vao file app/ App.php gan thuoc tinh o buoc 4 =  $this->__controller->db = new DB;
lam nhu nay thi cac controller con ke thua controller da co the khai bao va su dung lop new DB;
suy cho cung cung la lam cach nao de import duoc class DB de su dung no o moi noi.
*/
?>