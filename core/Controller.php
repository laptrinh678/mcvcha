<?php 
/*
base controller để các controller con kế thừa và sử dụng lại các hàm của controller cha này.
hàm loadmodel
hàm loadview
*/

class Controller
{
    public $model;
    public $db;
    public function LoadModel($model)
    {
        if(file_exists(__DIR_ROOR.'/app/models/'.$model.'.php'))
        {
            require_once(__DIR_ROOR.'/app/models/'.$model.'.php');
            if(class_exists($model))
            {
                $this->model = new $model;
                return $this->model;
            }
        }else{
            return false;
        }
    }
    public function loadView($url, $data=[])
    {
        extract($data);//chuyen key cua mang thanh bien
        if(file_exists(__DIR_ROOR.'/app/view/'.$url.'.php'))
        {
            require_once(__DIR_ROOR.'/app/view/'.$url.'.php');
        }else{
            echo 'khong tim thay view'.$url;
        }
    }
}

?>