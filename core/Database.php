<?php
class Database
{
	public  $__conn;
	public function __construct()
	{
		global $db_config;
		$this->__conn = Connection::getInstance($db_config);
	}
	public function query($sql)
	{
		$stament = $this->__conn->prepare($sql);
		$stament->execute();
		return $stament->fetchAll(PDO::FETCH_ASSOC);
	}
	public function delete2($sql)
	{ 
		try{
			$stament = $this->__conn->exec($sql);
			return true;
		 }catch (PDOException $e) 
			{
				$this->loadError($e->getMessage());
				//return 'Lỗi' . "<br>" . $e->getMessage();
			}
	}
	public function insertdata2($sql)
    {
		try{
			$stament = $this->__conn->exec($sql);
			//$stament->execute();
			return true;
		 }catch (PDOException $e) 
		{
			return 'Lỗi' . "<br>" . $e->getMessage();
		}
    }
	public function update2($sql)
	{

		try{
			$stmt = $this->__conn->prepare($sql);
		    $stmt->execute();
			return true;
		 }catch (PDOException $e) 
			{
				$this->loadError($e->getMessage());
			}
	}
} 