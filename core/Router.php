<?php 
/**
 * nhiệm vụ kiểm tra url(ảo) khi người dùng nhập vào trên thanh địa chỉ
 * nếu trong file route có khai báo route rồi thì sẽ so khớp để lấy ra đường dẫn file thật
 * nếu chưa có thì báo lỗi;
 */
class Router 
{
	
	public function hanlderRoute($url)
	{
		global $route;
		unset($route['default_controller']);
		$url = trim($url,'/');
		$hanlderUrl ='';
		foreach ($route as $key => $value)
		 {
			if(preg_match('~'.$key.'~is',$url))
			{
				$hanlderUrl = preg_replace('~'.$key.'~is', $value, $url);
			}
		 }
		return $hanlderUrl;
	}
}