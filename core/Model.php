<?php 
abstract class Model extends Database
{
    protected $db;
    use QueryBuilder;
    public function __construct()
    {
        $this->db = new Database;
       
    }
    abstract function tableFiels();
    abstract function findFiels();
    //abstract function findUpdate();
    public function get()
    {
        
        // $a = $this->tabel('users2')->select('id, email, password')->where('id','=','1')->where('name','=','lap')->getAll();//''->select('id')//->get();
        $sql =  $this->tabel('users2')->select('*')->orDerBy('id','desc')->limit(20)->getAll();//'SELECT * FROM '.$tabel;
       // $sql = $this->tabel('users')->select('*')->leftJoin('coupons','users.user_id = coupons.user_id')->getAll();
        $data = $this->db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
       // dd($data);
        return $data;
    }
    public function findById($tabel,$id)
    {
        $sql = $this->tabel($tabel)->select('*')->where('id','=', $id)->getAll();
        //dd($sql);
        $data = $this->db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
    public function delete($tabel,$id)
    {
        $sql = 'DELETE FROM '. $tabel. ' WHERE '.'id'. '='.$id;
        $data = $this->db->delete2($sql);
        return $data;
    }
    public function insert($request, $tabel)
    { 
        $col_request = implode (",",array_keys($request)); 
        $arr =[];
        foreach($request as $v)
        {
            $arr[] = "'".$v."'";
        }
        $data_request = implode (",",$arr);
        //sql mẫu $sql = "INSERT INTO users2 (email, password) VALUES ('laptrinh6789@gmail.com','123123')";
        $sql = "INSERT INTO ".  $tabel. ' ('.$col_request.') '. ' VALUES '. '('. $data_request .')';
		$re = $this->db->insertdata2($sql);
        return $re;
    }
    public function update($tabel='', $request, $col_where='')
    {
        //dd($request['id']);
        $arr = [];
       foreach($request as $key=>$v)
       {
           $data = $key.'=' . "'".$v."'";
           $arr[] = $data;
       }
       $col_request = implode (",",$arr); 
      // dd($col_request);
        //$sql = "UPDATE " .$tabel. " SET ". 'email'.'='."'habuon8'".','.'password'.'='.'000' .' WHERE '. 'id='. 12;
                  //UPDATE users2 SET 'email'='habuon5','password'='0','id'='12' WHERE id=12
                  //UPDATE users2 SET email='habuon8',password=000 WHERE id=12
        if($col_where ==''){
            $sql = "UPDATE " .$tabel. " SET ". $col_request . ' WHERE '.'id='.$request['id'];
        }else{
            $sql = "UPDATE " .$tabel. " SET ". $col_request . ' WHERE '.$col_where.'='.$request['id'];
        }
        //dd($sql);
        $re = $this->db->update2($sql);
        return $re;
    }
    public function search($tabel,$request)
    {
        //dd($request['email']);
        //SELECT * FROM `users2` WHERE email LIKE '%lap%';
        //SELECT * FROM users2 WHERE email LIKE %adf%
       $sql = $this->tabel($tabel)->select('*')->like('email','%'.$request['email'].'%')->getAll();
       //dd($sql);
       $re = $this->db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
       return $re;
       //dd($re);
    }
    

}
?>
